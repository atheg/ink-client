import React, { Component, PropTypes } from 'react';

const SignInButton = (props) => {

  if(props.authInProgress === false) {
    return (
      <button className="action-button" type="submit">sign in</button>
    );
  } else {
    return (
      <button className="action-button action-button--disabled">signing in...</button>
    );
  }
};

SignInButton.propTypes = {
  authInProgress: PropTypes.bool.isRequired,
  handleSignIn: PropTypes.func.isRequired
};

export default SignInButton;
