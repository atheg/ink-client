import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export class ExecutionPlaceholder extends Component {

  render() {
    return(
      <div className="processing-centered">
        <div><span className="fa fa-gear fa-spin fa-3x fa-fw" /></div>
        <div><span className="small-info">Starting...</span></div>
      </div>
    );
  }
}

ExecutionPlaceholder.propTypes = {
};

export default ExecutionPlaceholder;
