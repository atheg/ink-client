import React, { Component, PropTypes } from 'react';
import * as actions from '../actions/authenticationActions';
import _ from 'lodash';

export class RecipeStepDetail extends Component {

  renderStepDescription(description) {
    if(_.isEmpty(description)) {
      return(<span className="small-info-text">Cannot find the description for this step class! Are you sure it is spelled correctly?</span>);
    }
    return(description);
  }

  stepShorthand(stepName) {
    let array = stepName.split("::");
    return array[array.length - 1];
  }

  renderStepShorthand(stepName) {
    return(<div><span>{this.stepShorthand(stepName)}</span></div>);
  }

  allStepNameParts(stepName) {
    let array = stepName.split("::");
    if(array[0] == "InkStep") {
      array.shift();
    }
    return array;
  }

  renderStepExecutionParameters(executionParameters) {
    if(_.isNil(executionParameters) || _.isEmpty(executionParameters)) {
      return(
        <div>
          <span className="small-info-text">No parameters</span>
        </div>
      );
    }
    return(
      <div>
        <table className="table">
          <thead><tr><th>key</th><th>value</th></tr></thead>
          <tbody>
            {executionParameters && Object.keys(executionParameters).map(function(key, idx) {
                return <tr key={idx}><td>{key}</td><td>{executionParameters[key]}</td></tr>;
            }.bind(this))}
          </tbody>
        </table>
      </div>
    );
  }

  renderStepSummary(stepName) {
    const { human_readable_name } = this.props.recipeStep;

    let array = this.allStepNameParts(stepName);
    return(
      <div>
        <div className="step-module-container">
          { array.map((bit, index) => {
            return(<span key={index} className="step-module">{bit}</span>);
          })}
        </div>
        <div className="step--blue step__title">{human_readable_name}</div>
      </div>
    );
  }

  render() {
    const {position, step_class_name, description, execution_parameters} = this.props.recipeStep;

    let full_step_class_name = step_class_name;

    return(
      <div>
        <div className="step step--blue step__number">{position}</div>
        <div className="step">{this.renderStepSummary(step_class_name)}</div>
        <div className="step step--blue step__description">{this.renderStepDescription(description)}</div>
        <div className="step step__execution-parameters">
          <div><span className="fa fa-sliders"/> parameters</div>
          {this.renderStepExecutionParameters(execution_parameters)}
        </div>
      </div>
    );
  }
}

RecipeStepDetail.propTypes = {
  recipeStep: PropTypes.object
};

export default RecipeStepDetail;
