import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import RecipeStepDetail from './RecipeStepDetail';

import * as actions from '../actions/recipeActions';

export class EditRecipeForm extends Component {

  handleSubmission = (e) => {
    e.preventDefault();
    const form_data = new FormData();
    const { dispatch, appState, recipe } = this.props;

    form_data.append('recipe[name]', this.refs.recipe_name.value);
    form_data.append('recipe[description]', this.refs.recipe_description.value);
    form_data.append('recipe[public]', recipe.public);

    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.editRecipe(this.props.recipe.id, form_data, signedIn, authToken, tokenType, client, expiry, uid));
    browserHistory.push(`/recipes/`);
  }

  handleCancel = (e) => {
    const { recipe } = this.props;
    e.preventDefault();
    browserHistory.push(`/recipes/${recipe.id}`);
  }

  render() {
    let { recipe } = this.props;
    console.log("recipe to edit:", recipe)

    return(
      <form ref="editRecipeForm">
        <div className="form-input-container">
          <div className="form-input-labels">
            <div className="form-label">name</div>
            <div className="form-label">description</div>
          </div>
          <div className="form-input-boxes">
            <input className="input input--single-line-box" type="text" label="Name" ref="recipe_name" id="recipe_name" defaultValue={recipe.name} />
            <input className="input input--multi-line-box" type="textarea" cols="20" rows="40" label="Description" ref="recipe_description" id="recipe_description" defaultValue={recipe.description} />
          </div>
        </div>
        <div className="form-input-container">
          <div className="form-input-labels">
            <div className="form-label">step classes</div>
          </div>
          <div className="form-input-boxes">
            {recipe.recipe_steps.map(recipeStep =>
              <RecipeStepDetail
                key={recipeStep.id}
                recipeStep={recipeStep}
              />)
            }
          </div>
        </div>
        <div className="submit-button-container">
          <button className="action-button" onClick={this.handleSubmission}>Save</button>
          <button className="action-button action-button--cancel" onClick={this.handleCancel}>Cancel</button>
        </div>
      </form>
    );
  }
}

EditRecipeForm.propTypes = {
  recipe: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditRecipeForm);
