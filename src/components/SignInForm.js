import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/authenticationActions';
import SignInButton from './SignInButton';

import { getAllRecipes } from '../actions/recipeActions.js';

export class SignInForm extends React.Component {

  handleSignIn = (e) => {
    e.preventDefault();
    const email = this.refs.email.value;
    const password = this.refs.password.value;

    const { dispatch, appState } = this.props;
    dispatch(actions.fetchSignIn(email, password, appState));
  }

  render() {
    const { appState } = this.props;
    return (
      <div className="sign-in-form-container">
        <form onSubmit={this.handleSignIn}>
          <div className="form-input-container">
            <div className="form-input-labels">
              <div className="form-label">email</div>
              <div className="form-label">password</div>
            </div>
            <div className="form-input-boxes">
              <input type="text" ref="email" className="input input--sign-in" />
              <input type="password" ref="password" className="input input--sign-in" />
            </div>
          </div>
          <div className="sign-in-button-container">
            <SignInButton handleSignIn={this.handleSignIn} authInProgress={appState.authInProgress} />
          </div>
        </form>
      </div>
    );
  }
}

SignInForm.propTypes = {
  appState: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInForm);
