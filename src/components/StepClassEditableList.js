import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions/recipeActions';

export class StepClassEditableList extends Component {

  componentDidMount = () => {
  const { dispatch } = this.props;
    dispatch(actions.resetStepList);
  }

  handleRemove = (e) => {
    e.preventDefault();
    const { appState, dispatch, index } = this.props;
    const { stepClassList } = appState;
    dispatch(actions.removeFromStepList(index));
  }

  render() {
    const { appState, index, stepClassName } = this.props;
    return (
      <li className="stepClassListItem">
        <span className="list-item--step-class-name">{stepClassName}</span>
        <span className="pull-right left-pad">
          <a href="#" onClick={this.handleRemove}><span className="fa fa-times"/></a>
        </span>
      </li>
    );
  }
}

StepClassEditableList.propTypes = {
  appState: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  stepClassName: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

export default connect(
  mapStateToProps
)(StepClassEditableList);
