import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { favouriteRecipe, unfavouriteRecipe } from '../actions/recipeActions.js';
import * as actions from '../actions/recipeActions';

export class FavouriteWidget extends Component {

  favouriteRecipe = (e, recipeId) => {
    e.preventDefault();
    const { dispatch, appState } = this.props;
    const { authToken, tokenType, client, expiry, uid } = appState.session;
    const signedIn = (authToken != null);
    dispatch(actions.favouriteRecipe(recipeId, signedIn, authToken, tokenType, client, expiry, uid));
  }

  unfavouriteRecipe = (e, recipeId) => {
    e.preventDefault();
    const { dispatch, appState } = this.props;
    const { authToken, tokenType, client, expiry, uid } = appState.session;
    const signedIn = (authToken != null);
    dispatch(actions.unfavouriteRecipe(recipeId, signedIn, authToken, tokenType, client, expiry, uid));
  }

  render() {
    let { recipe } = this.props;
    let starClass = recipe.favourite == true ? "fa-star" : "fa-star-o";

    if(recipe.pendingFavourite == true) {
      return(
        <span className={`fa ${starClass} silver`} />
      );
    }
    if(recipe.favourite == true) {
      return(
        <a href={`#id=${recipe.id}`} onClick={e => this.unfavouriteRecipe(e, recipe.id)}><span className={`favourite fa ${starClass}`} /></a>
      );
    }
    else {
      return(
        <a href={`#id=${recipe.id}`} onClick={e => this.favouriteRecipe(e, recipe.id)}><span className={`favourite fa ${starClass}`} /></a>
      );
    }
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

FavouriteWidget.propTypes = {
  recipe: PropTypes.object.isRequired,
  appState: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouriteWidget);
