import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import TimeAgo from 'react-timeago';

import _ from 'lodash';
import * as actions from '../../../actions/adminActions.js';
import { setAlert } from '../../../actions/actions_helper';

import Header from '../../../components/header/Header';
import AlertList from '../../../components/AlertList';

class AccountShowPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object,
    account: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount = () => {
    let { dispatch, appState, account, params } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    if(_.isNull(appState.admin.accounts) || _.isNull(account)) {
      dispatch(actions.getAllAccounts(appState));
    }
  }

  findAccount() {
    let { appState, params, dispatch } = this.props;
    let accountId = _.toNumber(this.props.params.id);

    let account = _.find(appState.admin.accounts, _.matchesProperty('id', accountId));
    if(!_.isNil(account)) {
      return(account);
    }
  }

  renderEditLink(account) {
    const { session } = this.props.appState;
    if(account.account_id == session.account.id) {
      return(
        <Link to={`/admin/accounts/${account.id}/edit`}><span className="fa fa-pencil"/> edit this account</Link>
      );
    }
  }

  renderLastSignIn(date) {
    if(_.isNil(date)) {
      return(<span>Never</span>);
    } else {
      return(<span><TimeAgo date={date} /></span>);
    }
  }

  currentAccountLink(account) {
    return(`/admin/accounts/${account.id}`);
  }

  content(account, appState) {
    if(_.isEmpty(appState.admin.accounts)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-gear fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">Loading...</span></div>
        </div>
      );
    }

    if (_.isEmpty(account)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-question fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">That account might not exist...</span></div>
          <div><Link to="/admin/accounts">back to accounts list</Link></div>
        </div>
      );
    }

    return (
      <div className="content-container">
        <div className="breadcrumb-container">
          <Link to="/" className="breadcrumb">Home</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to="/accounts" className="breadcrumb"><span className="fa fa-users-o"/> Accounts</Link>
          <span className="breadcrumb-divider">&gt;&gt;</span>
          <Link to={this.currentAccountLink(account)} className="breadcrumb">{account.name}</Link>
        </div>

        <AlertList
          alerts={appState.alerts} />

        <div className="account-detail-view">
          <h1>Account: {account.name}</h1>
          <p><span className="fa fa-envelope"/> {account.email}</p>
          <p>Member since: {account.created_at}</p>
          <p>Last sign in: {this.renderLastSignIn(account.last_sign_in_at)}</p>
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    let account = this.findAccount();

    return (
      <div>
        <Header
          appState={appState}
        />
        {this.content(account, appState)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountShowPage);
