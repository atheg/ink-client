import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import * as actions from '../actions/authenticationActions';

import SignInForm from '../components/SignInForm';
import Header from '../components/header/Header';

import RecipesList from '../components/RecipesList';
import ExecutionList from '../components/ExecutionList';
import AlertList from '../components/AlertList';
import { getAllRecipes } from '../actions/recipeActions.js';

class HomePage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  render() {
    const { appState } = this.props;
    return (
      <div>
        <Header
          appState={appState}
        />
        <div className="content-container">
          <AlertList
            alerts={appState.alerts} />
          <div>
            <b><Link to="/recipes" href="#"><span className="fa fa-calendar-o"/> Recipes index</Link></b>
          </div>
          <div className="home-page-content-container">
            <div className="logo-container">
              <div><span className="logo logo__large">INK</span>
              <span className="small-info-text">alpha</span></div>
            </div>
            <h2>A fully customisable process pipeline <br/>
            framework for content publishing</h2>
            <h5>
              Automate processes for delivering great content!<br/>
              Fully customisable, this intentionally open-ended framework allows you<br/>
              to transform, convert, or analyse documents.<br/>
              You can use logic that others have shared as part of a sequence, or <br/>
              you can create your own that delivers value to you.
            </h5>
            <div className="action-button-container">
              <a className="curved-button" target="_blank" href="http://coko.foundation/all-about-ink-explained-with-cake/">Want to know more?</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
