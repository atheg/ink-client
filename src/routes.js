import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';

import HomePage from './containers/HomePage.js';
import NotFoundPage from './components/NotFoundPage.js';

import SignInPage from './containers/SignInPage.js';
import AdminDashboardPage from './containers/admin/dashboard.js';
import AccountShowPage from './containers/admin/accounts/show.js';

import RecipeIndexPage from './containers/recipes/index.js';
import RecipeShowPage from './containers/recipes/show.js';
import RecipeEditPage from './containers/recipes/edit.js';
import NewRecipePage from './containers/recipes/new.js';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />

    <Route path="admin/dashboard" component={AdminDashboardPage} />
    <Route path="admin/accounts/:id" component={AccountShowPage} />

    <Route path="recipes/new" component={NewRecipePage} />
    <Route path="recipes/:id" component={RecipeShowPage} />
    <Route path="recipes/:id/edit" component={RecipeEditPage} />
    <Route path="recipes" component={RecipeIndexPage}/>

    <Route path="signin" component={SignInPage}/>

    <Route path="*" component={NotFoundPage} />
  </Route>
);
