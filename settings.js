var INK_API_VERSION = 'v1';

// module.exports = {
//   apiBaseUrl : process.env.NODE_ENV == 'development' ?  'http://localhost:8080' : 'http://ink-api.coko.foundation',
//   apiVersionHeader : "application/vnd.ink.v1"
// }

module.exports = {
    apiBaseUrl : apiUrl(),
    apiVersionHeader : versionHeader()
}

function versionHeader() {
  return(`application/vnd.ink.${INK_API_VERSION}`);
}

function apiUrl() {
  var stage = process.env.STAGE;
  if(process.env.NODE_ENV == 'development') {
    return('http://localhost:8080');
  } else if(stage == 'production') {
    return('http://ink-api.coko.foundation');
  } else if(stage == 'staging') {
    return('http://ink-api-staging.coko.foundation');
  } else if(stage == 'demo') {
    return('http://ink-api-demo.coko.foundation');
  } else if(stage == 'test') {
    return('http://localhost:8080');
  } else {
    throw new Error("Stage " + stage + " has no matching API address");
  }
}
